<?php
namespace DBisso\Plugin\ManageMedia;

use Phly\Mustache\Mustache;
use Phly\Mustache\Pragma\ImplicitIterator;

/**
 * Class DBisso\Plugin\ManageMedia\Plugin
 */
class Plugin {
	static $_hooker;
	static $mustache;

	static public function bootstrap( $hooker = null ) {
		if ( $hooker ) {
			if ( !method_exists( $hooker, 'hook' ) )
				throw new \BadMethodCallException( 'Class ' . get_class( $hooker ) . ' has no hook() method.', 1 );

			self::$_hooker = $hooker;
			self::$_hooker->hook( __CLASS__, 'dbisso-manage-media' );
		} else {
			throw new \BadMethodCallException( 'Hooking class for ' . __CLASS__ . ' not specified.' , 1 );
		}

		register_activation_hook( __FILE__, array( __CLASS__, 'activation' ) );

		self::$mustache = new Mustache();
		self::$mustache->setTemplatePath( __DIR__ . '/templates/' );
		self::$mustache->getRenderer()->addPragma( new ImplicitIterator );
	}

	static public function activation() {}

	static public function action_add_meta_boxes() {
		foreach ( get_post_types() as $post_type ) {
			add_meta_box( 'dbisso-managemedia', __( 'Media', 'dbisso-manage-media' ), array( __CLASS__, 'meta_box_media' ), $post_type, 'advanced', 'default', array() );
		}
	}

	static public function meta_box_media( $post ) {
		$images = array();

		wp_register_script( 'dbisso-mustache', plugins_url( 'ManageMedia/js/mustache.js', __DIR__ ) );
		wp_enqueue_style( 'dbisso-manage-media-metabox', plugins_url( 'ManageMedia/css/dbisso-manage-media-metabox.css', __DIR__ ), array(), microtime() );
		wp_enqueue_script( 'dbisso-manage-media-metabox', plugins_url( 'ManageMedia/js/dbisso-manage-media-metabox.js', __DIR__ ), array( 'dbisso-mustache' ), microtime() );

		foreach ( self::fetch_media( $post ) as $key => $post ) {
			$images[] = array(
				'post' => (array) $post,
				'src_thumbnail' => wp_get_attachment_image( $post->ID, array( 50, 50 ) )
			);
		}

		$view = new \stdClass;
		$view->images = $images;

		echo self::$mustache->render( 'meta-box-media', $view );
		?>
		<p>
		 	<input type="button" class="dbisso-managemedia-mb--add button" name="add-resource" id="add-resource" value="<?php esc_attr_e( 'Edit Gallery', 'dbisso-manage-media' ) ?>" />
		 	<input type="hidden" id="resource-id" name="spliced_resource_id" value="" />
		</p>
		<script id="dbisso-managemedia-mb--tpl-test" type="text/mustache">
			<p>A template with a variable: {{thing}}</p>
		</script>
		<div style="clear: both"></div>
		<?php
	}

	static public function action_wp_ajax_dbisso_managemedia_fetch_attachments() {
		$post_id = (int) $_REQUEST['id'];

		$attachments = self::fetch_attachments( $post_id );

		wp_send_json_success(
			array_map( 'wp_prepare_attachment_for_js', $attachments )
		);
	}

	static public function fetch_attachments( $post_id ) {
		$meta    = get_post_meta( $post_id, 'dbisso_managemedia', true );

		if ( isset( $meta['attachments'] ) && is_array( $meta['attachments'] ) ) {
			$attachments = $meta['attachments'];
		} else {
			$attachments = wp_list_pluck( self::fetch_media( $post_id ), 'ID' );
		}

		return $attachments;
	}

	static public function action_wp_ajax_dbisso_managemedia_save_attachments() {
		$post_id = (int) $_REQUEST['id'];
		$attachments = array_map( 'intval', $_REQUEST['attachments'] );

		$meta = get_post_meta( $post_id, 'dbisso_managemedia', true );
// error_log( print_r($meta, true) ) ;
		$meta['attachments'] = $attachments;
		update_post_meta( $post_id, 'dbisso_managemedia', $meta );
		wp_send_json_success();
	}

	static private function fetch_media( $post ) {
		$post = get_post( $post ); // in case we receive a post ID
		$images = get_children(
			array(
				'post_type' => 'attachment',
				'post_parent' => $post->ID,
				'post_status' => 'inherit',
				'post_mime_type' => 'image',
				'order' => 'ASC',
				'orderby' => 'menu_order ID',
			)
		);

		return array_values($images);
	}
}