;(function($, dbisso, m){
	dbisso.plugin = window.dbisso.plugin || {};

	var plugin = dbisso.plugin.manageMedia = {};

	plugin.vent = _.extend({}, Backbone.Events);

	plugin.models = {
		Attachment: wp.media.model.Attachment.extend({})
	};

	plugin.collections = {
		Attachments: wp.media.model.Attachments.extend({
			initialize: function(models, options) {
				console.log(this);
				wp.media.model.Attachments.prototype.initialize.apply(this, arguments);

				plugin.vent.bind("attachments:change", this.save, this);
			},
			save: function(models) {
				this.models = models;
				this.sync("update", models);
			},
			// url: 'admin-ajax.php?action=dbisso_managemedia_fetch_attachments'
			sync: function(method, model, options) {
				// If the attachment does not yet have an `id`, return an instantly
				// rejected promise. Otherwise, all of our requests will fail.
				// if ( _.isUndefined( this.id ) )
					// return $.Deferred().rejectWith( this ).promise();

				// Overload the `read` request so Attachment.fetch() functions correctly.
				if ( 'read' === method ) {
					options = options || {};
					options.context = this;
					options.data = _.extend( options.data || {}, {
						action: 'dbisso_managemedia_fetch_attachments',
						id: wp.media.view.settings.post.id
					});

					return wp.media.ajax( options );
				}

				if ( 'update' === method ) {
					options = options || {};
					options.context = this;
					options.data = _.extend( options.data || {}, {
						action: 'dbisso_managemedia_save_attachments',
						id: wp.media.view.settings.post.id,
						attachments: _.pluck(model, 'id')
					});

					return wp.media.ajax( options );
				}

				return Backbone.Sync(method, model, options);
			},
			parse: function( resp, xhr ) {
				var attachments = _.map( resp, function( attrs ) {
					var attachment = plugin.models.Attachment.get( attrs.id );
					return attachment.set( attachment.parse( attrs, xhr ) );
				});

				plugin.vent.trigger("attachments:change", attachments);

				return attachments;
			},

		})
	};

	plugin.templates = {
		loaded: {},
		get: function(id) {
			if (_.isUndefined(plugin.templates.loaded[id])) {
				template = $('script#' + id).html();

				if (template.length > 0) {
					plugin.templates.loaded[id] = template;
					return template;
				}

				return false;
			}
		}
	};

	util = {
		nsHtml: 'dbisso-managemedia-mb',
		nsAttr: function(attr, sub) {
			return util.nsHtml + '--' + attr + ( !_.isUndefined(sub) ? '--' : '' );
		},
		nsId: function(attr, sub) {
			return '#' + util.nsAttr(attr, sub);
		},
		nsClass: function(attr, sub) {
			return '.' + util.nsAttr(attr, sub);
		}
	};

	_.extend( plugin, {
		mediaFrame: {},
		rootView: {},
		views: {
			metaBoxView: Backbone.View.extend({
				el: $('#dbisso-managemedia'),
				views: {},
				attachments: {},
				initialize: function() {
					this.attachments = new plugin.collections.Attachments();
					this.attachments.fetch({ post_id: wp.media.view.settings.post.id }, {wait: true});

					listView = this.views.listView = new plugin.views.imageListView({
						el: this.$el.find(util.nsClass("list")),
						collection: this.attachments
					});

					listView.render();

					this.views.editGalleryButton = new plugin.views.editGalleryButtonView({
						el: this.$el.find(util.nsClass("add")),
					});
				}
			}),
			imageListView: Backbone.View.extend({
				initialize: function() {
					this.template = plugin.templates.get(util.nsAttr("tpl-test"));
					plugin.vent.bind("attachments:change", this.updateAttachments, this);
				},
				events: {},
				updateAttachments: function(models) {
					this.render();
				},
				render: function() {
					var view = this;
					view.$el.empty();
					_.each( this.collection.models, function(model) {
						var image = new plugin.views.imageView({model: model});
						view.$el.append(image.render().el);
					});

					return this;
				},
			}),
			imageView: Backbone.View.extend({
				tagName: 'li',
				initialize: function(options) {
					this.className = util.nsClass('item');
					this.model.bind('change', this.render);
				},
				render: function() {
					var src = this.model.get('sizes').thumbnail.url
					$(this.el).html('<img src="' + src + '"/>');
					return this;
				}
			}),
			editGalleryButtonView: Backbone.View.extend({
				events: {
					click: "openMediaFrame"
				},
				openMediaFrame: function( event ) {
					event.preventDefault();
					plugin.mediaFrame.open();

					// If the selection is empty, load in the one from the metabox
					if ( plugin.mediaFrame.state().get('selection').length === 0 ) {

						// currentSelection = plugin.rootView.views.listView().collection;
						plugin.mediaFrame.state('gallery-edit').get('library').add( plugin.rootView.attachments.models );
						plugin.mediaFrame.state().trigger('reset');
						plugin.mediaFrame.setState('gallery-edit');
					}
				}
			})
		},

		initDom: function() {
			this.rootView = new plugin.views.metaBoxView();

			// When an image is selected, run a callback.
			plugin.mediaFrame.on( 'update', function( library ) {
				var attachments = library.models;
				plugin.vent.trigger("attachments:change", attachments);
			});
		},
		initMediaFrame: function() {
			// Create the media frame.
			plugin.mediaFrame = wp.media.frames.file_frame = wp.media({
				frame: 'post',
				state: 'gallery-library',
				title: jQuery( this ).data( 'uploader_title' )
			});
		},
		init: function() {
			plugin.initMediaFrame();

			$(function(){
				plugin.initDom();
			});
		}
	} );

	plugin.init();

})(jQuery, dbisso = window.dbisso || {}, Mustache);