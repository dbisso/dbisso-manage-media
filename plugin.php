<?php
/*
Plugin Name: QuickMedia
Description: Shows uploaded images in the post edit screen.
Version: 0.1
Author: Dan Bissonnet
Author URI: http://danisadesigner.com
*/

/**
 * Copyright (c) 2013 Dan Bissonnet. All rights reserved.
 *
 * Released under the GPL license
 * http://www.opensource.org/licenses/gpl-license.php
 *
 * This is an add-on for WordPress
 * http://wordpress.org/
 *
 * **********************************************************************
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * **********************************************************************
 */

$autoload_path = '/vendor/autoload.php';

if ( file_exists( __DIR__ . $autoload_path ) ) {
	$loader = include __DIR__ . $autoload_path;
} else if ( file_exists( ABSPATH . $autoload_path ) ) {
	$loader = include ABSPATH . $autoload_path;
} else if ( file_exists( ABSPATH . '..' . $autoload_path ) ) {
	$loader = include ABSPATH . '..' . $autoload_path;
} else {
	wp_die( 'Could not find composer autoloader' );
}

$loader->add( 'DBisso', __DIR__ . '/lib' );

/**
 * Bootstrap or die
 */
try {
	if ( class_exists( '\DBisso\Util\Hooker' ) ) {
		DBisso\Plugin\ManageMedia\Plugin::bootstrap( new DBisso\Util\Hooker );
	} else {
		throw new \Exception( 'Class DBisso\Util\Hooker not found. Check that the plugin is installed.', 1 );
	}
} catch ( \Exception $e ) {
	wp_die( $e->getMessage(), $title = 'Theme Exception' );
}
